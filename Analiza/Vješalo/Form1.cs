﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp8
{
	public partial class Form1 : Form
	{
		int n = 6, GameWordID, hits;
		string path = "C:\\rijeci.txt";
		List<string> Words = new List<string>();
		string GameWord, Unknown;
		System.Text.StringBuilder UnknownBuild;

		public Form1()
		{
			InitializeComponent();
			using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
			{
				string line;
				while ((line = reader.ReadLine()) != null)
				{
					Words.Add(line);
				}
			}
			NewGame();
		}


		private void button1_Click(object sender, EventArgs e)
		{
			bool hit = false;
			Try.Text = n.ToString();
			Button letter = (Button)sender;
			letter.Enabled = false;

			for (int i = 0; i < GameWord.Length; i++)
			{

				if (GameWord[i] == Char.ToLower(letter.Text[0]))
				{
					hits++;
					UnknownBuild[i * 2] = Char.ToLower(letter.Text[0]);
					Unknown = UnknownBuild.ToString();
					hit = true;
					continue;
				}
			}
			if (!hit)
			{
				n--;
				Try.Text = n.ToString();
			}
			Word.Text = Unknown;
			

			Win();
			Loss();
		}

		private void button23_Click(object sender, EventArgs e)
		{
			NewGame();
		}
		public void NewGame()
		{
			Unknown = "";
			hits = 0;
			n = 6;
			Random rnd = new Random();
			GameWordID = rnd.Next(0, Words.Count);
			GameWord = Words[GameWordID];
			for (int i = 0; i < GameWord.Length; i++)
			{
				Unknown += "_ ";
			}
			UnknownBuild = new StringBuilder(Unknown);
			Try.Text = n.ToString();
			Word.Text = Unknown;
			foreach (Control c in Controls)
			{
				Button b = c as Button;
				if (b != null)
				{
					b.Enabled = true;
				}
			}
		}
		public void Win()
		{
			if (hits == GameWord.Length)
			{
				MessageBox.Show("You won!");
				NewGame();
			}
		}
		public void Loss()
		{
			if (n <= 0)
			{

				MessageBox.Show($"Game over\nWord: {GameWord}");
				NewGame();
			}
		}

	}
}
