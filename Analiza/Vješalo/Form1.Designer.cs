﻿namespace WindowsFormsApp8
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Restart = new System.Windows.Forms.Button();
			this.button22 = new System.Windows.Forms.Button();
			this.button21 = new System.Windows.Forms.Button();
			this.button20 = new System.Windows.Forms.Button();
			this.button19 = new System.Windows.Forms.Button();
			this.button18 = new System.Windows.Forms.Button();
			this.button17 = new System.Windows.Forms.Button();
			this.button16 = new System.Windows.Forms.Button();
			this.button15 = new System.Windows.Forms.Button();
			this.button14 = new System.Windows.Forms.Button();
			this.button13 = new System.Windows.Forms.Button();
			this.button12 = new System.Windows.Forms.Button();
			this.button11 = new System.Windows.Forms.Button();
			this.button10 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button = new System.Windows.Forms.Button();
			this.Word = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.Try = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// Restart
			// 
			this.Restart.Location = new System.Drawing.Point(423, 30);
			this.Restart.Name = "Restart";
			this.Restart.Size = new System.Drawing.Size(87, 43);
			this.Restart.TabIndex = 47;
			this.Restart.Text = "Restart";
			this.Restart.UseVisualStyleBackColor = true;
			this.Restart.Click += new System.EventHandler(this.button23_Click);
			// 
			// button22
			// 
			this.button22.Location = new System.Drawing.Point(385, 54);
			this.button22.Name = "button22";
			this.button22.Size = new System.Drawing.Size(32, 28);
			this.button22.TabIndex = 46;
			this.button22.Text = "Z";
			this.button22.UseVisualStyleBackColor = true;
			this.button22.Click += new System.EventHandler(this.button1_Click);
			// 
			// button21
			// 
			this.button21.Location = new System.Drawing.Point(347, 54);
			this.button21.Name = "button21";
			this.button21.Size = new System.Drawing.Size(32, 28);
			this.button21.TabIndex = 45;
			this.button21.Text = "V";
			this.button21.UseVisualStyleBackColor = true;
			this.button21.Click += new System.EventHandler(this.button1_Click);
			// 
			// button20
			// 
			this.button20.Location = new System.Drawing.Point(309, 54);
			this.button20.Name = "button20";
			this.button20.Size = new System.Drawing.Size(32, 28);
			this.button20.TabIndex = 44;
			this.button20.Text = "U";
			this.button20.UseVisualStyleBackColor = true;
			this.button20.Click += new System.EventHandler(this.button1_Click);
			// 
			// button19
			// 
			this.button19.Location = new System.Drawing.Point(271, 54);
			this.button19.Name = "button19";
			this.button19.Size = new System.Drawing.Size(32, 28);
			this.button19.TabIndex = 43;
			this.button19.Text = "T";
			this.button19.UseVisualStyleBackColor = true;
			this.button19.Click += new System.EventHandler(this.button1_Click);
			// 
			// button18
			// 
			this.button18.Location = new System.Drawing.Point(233, 54);
			this.button18.Name = "button18";
			this.button18.Size = new System.Drawing.Size(32, 28);
			this.button18.TabIndex = 42;
			this.button18.Text = "S";
			this.button18.UseVisualStyleBackColor = true;
			this.button18.Click += new System.EventHandler(this.button1_Click);
			// 
			// button17
			// 
			this.button17.Location = new System.Drawing.Point(195, 54);
			this.button17.Name = "button17";
			this.button17.Size = new System.Drawing.Size(32, 28);
			this.button17.TabIndex = 41;
			this.button17.Text = "R";
			this.button17.UseVisualStyleBackColor = true;
			this.button17.Click += new System.EventHandler(this.button1_Click);
			// 
			// button16
			// 
			this.button16.Location = new System.Drawing.Point(157, 54);
			this.button16.Name = "button16";
			this.button16.Size = new System.Drawing.Size(32, 28);
			this.button16.TabIndex = 40;
			this.button16.Text = "P";
			this.button16.UseVisualStyleBackColor = true;
			this.button16.Click += new System.EventHandler(this.button1_Click);
			// 
			// button15
			// 
			this.button15.Location = new System.Drawing.Point(119, 54);
			this.button15.Name = "button15";
			this.button15.Size = new System.Drawing.Size(32, 28);
			this.button15.TabIndex = 39;
			this.button15.Text = "O";
			this.button15.UseVisualStyleBackColor = true;
			this.button15.Click += new System.EventHandler(this.button1_Click);
			// 
			// button14
			// 
			this.button14.Location = new System.Drawing.Point(81, 54);
			this.button14.Name = "button14";
			this.button14.Size = new System.Drawing.Size(32, 28);
			this.button14.TabIndex = 38;
			this.button14.Text = "N";
			this.button14.UseVisualStyleBackColor = true;
			this.button14.Click += new System.EventHandler(this.button1_Click);
			// 
			// button13
			// 
			this.button13.Location = new System.Drawing.Point(43, 54);
			this.button13.Name = "button13";
			this.button13.Size = new System.Drawing.Size(32, 28);
			this.button13.TabIndex = 37;
			this.button13.Text = "M";
			this.button13.UseVisualStyleBackColor = true;
			this.button13.Click += new System.EventHandler(this.button1_Click);
			// 
			// button12
			// 
			this.button12.Location = new System.Drawing.Point(5, 54);
			this.button12.Name = "button12";
			this.button12.Size = new System.Drawing.Size(32, 28);
			this.button12.TabIndex = 36;
			this.button12.Text = "L";
			this.button12.UseVisualStyleBackColor = true;
			this.button12.Click += new System.EventHandler(this.button1_Click);
			// 
			// button11
			// 
			this.button11.Location = new System.Drawing.Point(385, 20);
			this.button11.Name = "button11";
			this.button11.Size = new System.Drawing.Size(32, 28);
			this.button11.TabIndex = 35;
			this.button11.Text = "K";
			this.button11.UseVisualStyleBackColor = true;
			this.button11.Click += new System.EventHandler(this.button1_Click);
			// 
			// button10
			// 
			this.button10.Location = new System.Drawing.Point(347, 20);
			this.button10.Name = "button10";
			this.button10.Size = new System.Drawing.Size(32, 28);
			this.button10.TabIndex = 34;
			this.button10.Text = "J";
			this.button10.UseVisualStyleBackColor = true;
			this.button10.Click += new System.EventHandler(this.button1_Click);
			// 
			// button9
			// 
			this.button9.Location = new System.Drawing.Point(309, 20);
			this.button9.Name = "button9";
			this.button9.Size = new System.Drawing.Size(32, 28);
			this.button9.TabIndex = 33;
			this.button9.Text = "I";
			this.button9.UseVisualStyleBackColor = true;
			this.button9.Click += new System.EventHandler(this.button1_Click);
			// 
			// button8
			// 
			this.button8.Location = new System.Drawing.Point(271, 20);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(32, 28);
			this.button8.TabIndex = 32;
			this.button8.Text = "H";
			this.button8.UseVisualStyleBackColor = true;
			this.button8.Click += new System.EventHandler(this.button1_Click);
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(233, 20);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(32, 28);
			this.button7.TabIndex = 31;
			this.button7.Text = "G";
			this.button7.UseVisualStyleBackColor = true;
			this.button7.Click += new System.EventHandler(this.button1_Click);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(195, 20);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(32, 28);
			this.button6.TabIndex = 30;
			this.button6.Text = "F";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.button1_Click);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(157, 20);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(32, 28);
			this.button5.TabIndex = 29;
			this.button5.Text = "E";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.button1_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(119, 20);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(32, 28);
			this.button4.TabIndex = 28;
			this.button4.Text = "D";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button1_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(81, 20);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(32, 28);
			this.button3.TabIndex = 27;
			this.button3.Text = "C";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(43, 20);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(32, 28);
			this.button2.TabIndex = 26;
			this.button2.Text = "B";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button1_Click);
			// 
			// button
			// 
			this.button.Location = new System.Drawing.Point(5, 20);
			this.button.Name = "button";
			this.button.Size = new System.Drawing.Size(32, 28);
			this.button.TabIndex = 25;
			this.button.Text = "A";
			this.button.UseVisualStyleBackColor = true;
			this.button.Click += new System.EventHandler(this.button1_Click);
			// 
			// Word
			// 
			this.Word.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.Word.Location = new System.Drawing.Point(163, 100);
			this.Word.Name = "Word";
			this.Word.Size = new System.Drawing.Size(158, 42);
			this.Word.TabIndex = 24;
			this.Word.Text = "Word";
			this.Word.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label2.Location = new System.Drawing.Point(364, 110);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(82, 23);
			this.label2.TabIndex = 48;
			this.label2.Text = "Tries left: ";
			// 
			// Try
			// 
			this.Try.AutoSize = true;
			this.Try.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.Try.Location = new System.Drawing.Point(452, 110);
			this.Try.Name = "Try";
			this.Try.Size = new System.Drawing.Size(18, 20);
			this.Try.TabIndex = 49;
			this.Try.Text = "6";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(515, 157);
			this.Controls.Add(this.Try);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.Restart);
			this.Controls.Add(this.button22);
			this.Controls.Add(this.button21);
			this.Controls.Add(this.button20);
			this.Controls.Add(this.button19);
			this.Controls.Add(this.button18);
			this.Controls.Add(this.button17);
			this.Controls.Add(this.button16);
			this.Controls.Add(this.button15);
			this.Controls.Add(this.button14);
			this.Controls.Add(this.button13);
			this.Controls.Add(this.button12);
			this.Controls.Add(this.button11);
			this.Controls.Add(this.button10);
			this.Controls.Add(this.button9);
			this.Controls.Add(this.button8);
			this.Controls.Add(this.button7);
			this.Controls.Add(this.button6);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button);
			this.Controls.Add(this.Word);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button Restart;
		private System.Windows.Forms.Button button22;
		private System.Windows.Forms.Button button21;
		private System.Windows.Forms.Button button20;
		private System.Windows.Forms.Button button19;
		private System.Windows.Forms.Button button18;
		private System.Windows.Forms.Button button17;
		private System.Windows.Forms.Button button16;
		private System.Windows.Forms.Button button15;
		private System.Windows.Forms.Button button14;
		private System.Windows.Forms.Button button13;
		private System.Windows.Forms.Button button12;
		private System.Windows.Forms.Button button11;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button;
		private System.Windows.Forms.Label Word;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label Try;
	}
}

