﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Zad1 : Form
    {
		int n = 0, eq = 1;
       
        public Zad1()
        {
            InitializeComponent();
        }

        double rez = 0;
        string op = "";
        double x = 0;

		private void btn_0_Click(object sender, EventArgs e)
		{
			if (eq == 1)
			{
				eq = 0;
				rezultat.Text = "";
				rez = 0;
				x = 0;
			}
			if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text += "0";
			if (n != 0)
			{
				n = 0;
			}
		}

        private void btn_2_Click(object sender, EventArgs e)
        {
			if (eq == 1)
			{
				eq = 0;
				rezultat.Text = "";
				rez = 0;
				x = 0;
			}
			if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text += "2";
			if (n != 0)
			{
				n = 0;
			}
		}

        private void btn_1_Click(object sender, EventArgs e)
        {
			if (eq == 1)
			{
				eq = 0;
				rezultat.Text = "";
				rez = 0;
				x = 0;
			}
			if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text += "1";
			if (n != 0)
			{
				n = 0;
			}
		}

        

        private void btn_3_Click(object sender, EventArgs e)
        {
			if (eq == 1)
			{
				eq = 0;
				rezultat.Text = "";
				rez = 0;
				x = 0;
			}
			if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text += "3";
			if (n != 0)
			{
				n = 0;
			}
		}

        private void btn_4_Click(object sender, EventArgs e)
        {
			if (eq == 1)
			{
				eq = 0;
				rezultat.Text = "";
				rez = 0;
				x = 0;
			}
			if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text += "4";
			if (n != 0)
			{
				n = 0;
			}
		}

        private void btn_5_Click(object sender, EventArgs e)
        {
			if (eq == 1)
			{
				eq = 0;
				rezultat.Text = "";
				rez = 0;
				x = 0;
			}
			if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text += "5";
			if (n != 0)
			{
				n = 0;
			}
		}

        private void btn_6_Click(object sender, EventArgs e)
        {
			if (eq == 1)
			{
				eq = 0;
				rezultat.Text = "";
				rez = 0;
				x = 0;
			}
			if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text += "6";
			if (n != 0)
			{
				n = 0;
			}
		}

        private void btn_7_Click(object sender, EventArgs e)
        {
			if (eq == 1)
			{
				eq = 0;
				rezultat.Text = "";
				rez = 0;
				x = 0;
			}
			if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text += "7";
			if (n != 0)
			{
				n = 0;
			}
		}

        private void btn_8_Click(object sender, EventArgs e)
        {
			if (eq == 1)
			{
				eq = 0;
				rezultat.Text = "";
				rez = 0;
				x = 0;
			}
			if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text += "8";
			if (n != 0)
			{
				n = 0;
			}
		}

        private void btn_9_Click(object sender, EventArgs e)
        {
			if (eq == 1)
			{
				eq = 0;
				rezultat.Text = "";
				rez = 0;
				x = 0;
			}
			if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text += "9";
			if (n != 0)
			{
				n = 0;
			}
        }

        private void btn_CE_Click(object sender, EventArgs e)
        {
            rezultat.Text = "0";
            rez = 0;
            x = 0;
        }

        private void btn_plus_Click(object sender, EventArgs e)
        {
            if (n == 0)
                rez += double.Parse(rezultat.Text);
            op = "+";
            rezultat.Text = "";
            n++;
        }

        private void btn_dot_Click(object sender, EventArgs e)
        {
            string zarez = ",";
            string broj = rezultat.Text.ToString();
            bool cont = broj.Contains(zarez);
            if (!cont)
                rezultat.Text += ",";
			if (n != 0)
			{
				n = 0;
			}
		}

        private void btn_minus_Click(object sender, EventArgs e)
        {
            if(n==0)
                rez = double.Parse(rezultat.Text);
            op = "-";
            rezultat.Text = "-";
            n++;

        }

        private void btn_multiply_Click(object sender, EventArgs e)
        {
            if (n == 0)
                rez = double.Parse(rezultat.Text);
            op = "*";
            rezultat.Text = "";
            n++;
        }

        private void btn_log_Click(object sender, EventArgs e)
        {
            rez = Math.Log10(double.Parse(rezultat.Text));
            rezultat.Text = rez.ToString();
        }

        private void btn_sqrt_Click(object sender, EventArgs e)
        {
            rez = Math.Sqrt(double.Parse(rezultat.Text));
            rezultat.Text = rez.ToString();
        }

        private void btn_abs_Click(object sender, EventArgs e)
        {
            rez = Math.Abs(double.Parse(rezultat.Text));
            rezultat.Text = rez.ToString();
        }

        private void btn_devide_Click(object sender, EventArgs e)
        {
            if (n == 0)
                rez = double.Parse(rezultat.Text);
            op = "/";
            rezultat.Text = "";
            n++;
        }

        private void btn_eq_Click(object sender, EventArgs e)
        {
			eq = 1;
            x = double.Parse(rezultat.Text);
            if (op == "+")
                rezultat.Text = (rez + x).ToString();
            if (op == "-")
                rezultat.Text = (rez + x).ToString();
            if (op == "*")
                rezultat.Text = (rez * x).ToString();
            if (op == "/")
            {
                if (x == 0)
                    rezultat.Text = "Math Error";
                else
                    rezultat.Text = (rez / x).ToString();
            }
            n = 0;
            rez = double.Parse(rezultat.Text);
            
        }

        private void btn_sin_Click(object sender, EventArgs e)
        {
            rez = Math.Sin(double.Parse(rezultat.Text));
            rezultat.Text = rez.ToString();
        }

        private void btn_cos_Click(object sender, EventArgs e)
        {
            rez = Math.Cos(double.Parse(rezultat.Text));
            rezultat.Text = rez.ToString();
        }

		private void rezultat_TextChanged(object sender, EventArgs e)
		{

		}
	}
}
