﻿namespace WindowsFormsApp1
{
    partial class Zad1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.btn_7 = new System.Windows.Forms.Button();
			this.btn_8 = new System.Windows.Forms.Button();
			this.btn_9 = new System.Windows.Forms.Button();
			this.btn_6 = new System.Windows.Forms.Button();
			this.btn_5 = new System.Windows.Forms.Button();
			this.btn_4 = new System.Windows.Forms.Button();
			this.btn_3 = new System.Windows.Forms.Button();
			this.btn_2 = new System.Windows.Forms.Button();
			this.btn_1 = new System.Windows.Forms.Button();
			this.btn_0 = new System.Windows.Forms.Button();
			this.btn_eq = new System.Windows.Forms.Button();
			this.btn_CE = new System.Windows.Forms.Button();
			this.btn_plus = new System.Windows.Forms.Button();
			this.btn_minus = new System.Windows.Forms.Button();
			this.btn_multiply = new System.Windows.Forms.Button();
			this.btn_devide = new System.Windows.Forms.Button();
			this.btn_dot = new System.Windows.Forms.Button();
			this.btn_abs = new System.Windows.Forms.Button();
			this.btn_log = new System.Windows.Forms.Button();
			this.btn_cos = new System.Windows.Forms.Button();
			this.btn_sin = new System.Windows.Forms.Button();
			this.btn_sqrt = new System.Windows.Forms.Button();
			this.rezultat = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// btn_7
			// 
			this.btn_7.Location = new System.Drawing.Point(6, 60);
			this.btn_7.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_7.Name = "btn_7";
			this.btn_7.Size = new System.Drawing.Size(46, 39);
			this.btn_7.TabIndex = 1;
			this.btn_7.Text = "7";
			this.btn_7.UseVisualStyleBackColor = true;
			this.btn_7.Click += new System.EventHandler(this.btn_7_Click);
			// 
			// btn_8
			// 
			this.btn_8.Location = new System.Drawing.Point(56, 60);
			this.btn_8.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_8.Name = "btn_8";
			this.btn_8.Size = new System.Drawing.Size(46, 39);
			this.btn_8.TabIndex = 2;
			this.btn_8.Text = "8";
			this.btn_8.UseVisualStyleBackColor = true;
			this.btn_8.Click += new System.EventHandler(this.btn_8_Click);
			// 
			// btn_9
			// 
			this.btn_9.Location = new System.Drawing.Point(107, 60);
			this.btn_9.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_9.Name = "btn_9";
			this.btn_9.Size = new System.Drawing.Size(46, 39);
			this.btn_9.TabIndex = 3;
			this.btn_9.Text = "9";
			this.btn_9.UseVisualStyleBackColor = true;
			this.btn_9.Click += new System.EventHandler(this.btn_9_Click);
			// 
			// btn_6
			// 
			this.btn_6.Location = new System.Drawing.Point(107, 104);
			this.btn_6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_6.Name = "btn_6";
			this.btn_6.Size = new System.Drawing.Size(46, 39);
			this.btn_6.TabIndex = 6;
			this.btn_6.Text = "6";
			this.btn_6.UseVisualStyleBackColor = true;
			this.btn_6.Click += new System.EventHandler(this.btn_6_Click);
			// 
			// btn_5
			// 
			this.btn_5.Location = new System.Drawing.Point(56, 104);
			this.btn_5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_5.Name = "btn_5";
			this.btn_5.Size = new System.Drawing.Size(46, 39);
			this.btn_5.TabIndex = 5;
			this.btn_5.Text = "5";
			this.btn_5.UseVisualStyleBackColor = true;
			this.btn_5.Click += new System.EventHandler(this.btn_5_Click);
			// 
			// btn_4
			// 
			this.btn_4.Location = new System.Drawing.Point(6, 104);
			this.btn_4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_4.Name = "btn_4";
			this.btn_4.Size = new System.Drawing.Size(46, 39);
			this.btn_4.TabIndex = 4;
			this.btn_4.Text = "4";
			this.btn_4.UseVisualStyleBackColor = true;
			this.btn_4.Click += new System.EventHandler(this.btn_4_Click);
			// 
			// btn_3
			// 
			this.btn_3.Location = new System.Drawing.Point(107, 148);
			this.btn_3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_3.Name = "btn_3";
			this.btn_3.Size = new System.Drawing.Size(46, 39);
			this.btn_3.TabIndex = 9;
			this.btn_3.Text = "3";
			this.btn_3.UseVisualStyleBackColor = true;
			this.btn_3.Click += new System.EventHandler(this.btn_3_Click);
			// 
			// btn_2
			// 
			this.btn_2.Location = new System.Drawing.Point(56, 148);
			this.btn_2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_2.Name = "btn_2";
			this.btn_2.Size = new System.Drawing.Size(46, 39);
			this.btn_2.TabIndex = 8;
			this.btn_2.Text = "2";
			this.btn_2.UseVisualStyleBackColor = true;
			this.btn_2.Click += new System.EventHandler(this.btn_2_Click);
			// 
			// btn_1
			// 
			this.btn_1.Location = new System.Drawing.Point(6, 148);
			this.btn_1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_1.Name = "btn_1";
			this.btn_1.Size = new System.Drawing.Size(46, 39);
			this.btn_1.TabIndex = 7;
			this.btn_1.Text = "1";
			this.btn_1.UseVisualStyleBackColor = true;
			this.btn_1.Click += new System.EventHandler(this.btn_1_Click);
			// 
			// btn_0
			// 
			this.btn_0.Location = new System.Drawing.Point(56, 192);
			this.btn_0.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_0.Name = "btn_0";
			this.btn_0.Size = new System.Drawing.Size(46, 39);
			this.btn_0.TabIndex = 10;
			this.btn_0.Text = "0";
			this.btn_0.UseVisualStyleBackColor = true;
			this.btn_0.Click += new System.EventHandler(this.btn_0_Click);
			// 
			// btn_eq
			// 
			this.btn_eq.Location = new System.Drawing.Point(107, 192);
			this.btn_eq.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_eq.Name = "btn_eq";
			this.btn_eq.Size = new System.Drawing.Size(46, 39);
			this.btn_eq.TabIndex = 11;
			this.btn_eq.Text = "=";
			this.btn_eq.UseVisualStyleBackColor = true;
			this.btn_eq.Click += new System.EventHandler(this.btn_eq_Click);
			// 
			// btn_CE
			// 
			this.btn_CE.Location = new System.Drawing.Point(157, 192);
			this.btn_CE.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_CE.Name = "btn_CE";
			this.btn_CE.Size = new System.Drawing.Size(46, 39);
			this.btn_CE.TabIndex = 12;
			this.btn_CE.Text = "C";
			this.btn_CE.UseVisualStyleBackColor = true;
			this.btn_CE.Click += new System.EventHandler(this.btn_CE_Click);
			// 
			// btn_plus
			// 
			this.btn_plus.Location = new System.Drawing.Point(216, 61);
			this.btn_plus.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_plus.Name = "btn_plus";
			this.btn_plus.Size = new System.Drawing.Size(55, 23);
			this.btn_plus.TabIndex = 13;
			this.btn_plus.Text = "+";
			this.btn_plus.UseVisualStyleBackColor = true;
			this.btn_plus.Click += new System.EventHandler(this.btn_plus_Click);
			// 
			// btn_minus
			// 
			this.btn_minus.Location = new System.Drawing.Point(157, 61);
			this.btn_minus.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_minus.Name = "btn_minus";
			this.btn_minus.Size = new System.Drawing.Size(55, 23);
			this.btn_minus.TabIndex = 14;
			this.btn_minus.Text = "-";
			this.btn_minus.UseVisualStyleBackColor = true;
			this.btn_minus.Click += new System.EventHandler(this.btn_minus_Click);
			// 
			// btn_multiply
			// 
			this.btn_multiply.Location = new System.Drawing.Point(157, 90);
			this.btn_multiply.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_multiply.Name = "btn_multiply";
			this.btn_multiply.Size = new System.Drawing.Size(55, 23);
			this.btn_multiply.TabIndex = 15;
			this.btn_multiply.Text = "*";
			this.btn_multiply.UseVisualStyleBackColor = true;
			this.btn_multiply.Click += new System.EventHandler(this.btn_multiply_Click);
			// 
			// btn_devide
			// 
			this.btn_devide.Location = new System.Drawing.Point(216, 90);
			this.btn_devide.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_devide.Name = "btn_devide";
			this.btn_devide.Size = new System.Drawing.Size(55, 23);
			this.btn_devide.TabIndex = 16;
			this.btn_devide.Text = "/";
			this.btn_devide.UseVisualStyleBackColor = true;
			this.btn_devide.Click += new System.EventHandler(this.btn_devide_Click);
			// 
			// btn_dot
			// 
			this.btn_dot.Location = new System.Drawing.Point(6, 192);
			this.btn_dot.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_dot.Name = "btn_dot";
			this.btn_dot.Size = new System.Drawing.Size(46, 39);
			this.btn_dot.TabIndex = 17;
			this.btn_dot.Text = ",";
			this.btn_dot.UseVisualStyleBackColor = true;
			this.btn_dot.Click += new System.EventHandler(this.btn_dot_Click);
			// 
			// btn_abs
			// 
			this.btn_abs.Location = new System.Drawing.Point(216, 118);
			this.btn_abs.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_abs.Name = "btn_abs";
			this.btn_abs.Size = new System.Drawing.Size(55, 25);
			this.btn_abs.TabIndex = 21;
			this.btn_abs.Text = "abs";
			this.btn_abs.UseVisualStyleBackColor = true;
			this.btn_abs.Click += new System.EventHandler(this.btn_abs_Click);
			// 
			// btn_log
			// 
			this.btn_log.Location = new System.Drawing.Point(275, 118);
			this.btn_log.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_log.Name = "btn_log";
			this.btn_log.Size = new System.Drawing.Size(55, 25);
			this.btn_log.TabIndex = 20;
			this.btn_log.Text = "log";
			this.btn_log.UseVisualStyleBackColor = true;
			this.btn_log.Click += new System.EventHandler(this.btn_log_Click);
			// 
			// btn_cos
			// 
			this.btn_cos.Location = new System.Drawing.Point(216, 148);
			this.btn_cos.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_cos.Name = "btn_cos";
			this.btn_cos.Size = new System.Drawing.Size(55, 38);
			this.btn_cos.TabIndex = 19;
			this.btn_cos.Text = "cos";
			this.btn_cos.UseVisualStyleBackColor = true;
			this.btn_cos.Click += new System.EventHandler(this.btn_cos_Click);
			// 
			// btn_sin
			// 
			this.btn_sin.Location = new System.Drawing.Point(157, 148);
			this.btn_sin.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_sin.Name = "btn_sin";
			this.btn_sin.Size = new System.Drawing.Size(55, 38);
			this.btn_sin.TabIndex = 18;
			this.btn_sin.Text = "sin";
			this.btn_sin.UseVisualStyleBackColor = true;
			this.btn_sin.Click += new System.EventHandler(this.btn_sin_Click);
			// 
			// btn_sqrt
			// 
			this.btn_sqrt.Location = new System.Drawing.Point(157, 118);
			this.btn_sqrt.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.btn_sqrt.Name = "btn_sqrt";
			this.btn_sqrt.Size = new System.Drawing.Size(55, 25);
			this.btn_sqrt.TabIndex = 22;
			this.btn_sqrt.Text = "sqrt";
			this.btn_sqrt.UseVisualStyleBackColor = true;
			this.btn_sqrt.Click += new System.EventHandler(this.btn_sqrt_Click);
			// 
			// rezultat
			// 
			this.rezultat.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rezultat.Location = new System.Drawing.Point(6, 27);
			this.rezultat.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.rezultat.Name = "rezultat";
			this.rezultat.Size = new System.Drawing.Size(267, 30);
			this.rezultat.TabIndex = 23;
			this.rezultat.Text = "0";
			this.rezultat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.rezultat.TextChanged += new System.EventHandler(this.rezultat_TextChanged);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(350, 256);
			this.Controls.Add(this.rezultat);
			this.Controls.Add(this.btn_sqrt);
			this.Controls.Add(this.btn_abs);
			this.Controls.Add(this.btn_log);
			this.Controls.Add(this.btn_cos);
			this.Controls.Add(this.btn_sin);
			this.Controls.Add(this.btn_dot);
			this.Controls.Add(this.btn_devide);
			this.Controls.Add(this.btn_multiply);
			this.Controls.Add(this.btn_minus);
			this.Controls.Add(this.btn_plus);
			this.Controls.Add(this.btn_CE);
			this.Controls.Add(this.btn_eq);
			this.Controls.Add(this.btn_0);
			this.Controls.Add(this.btn_3);
			this.Controls.Add(this.btn_2);
			this.Controls.Add(this.btn_1);
			this.Controls.Add(this.btn_6);
			this.Controls.Add(this.btn_5);
			this.Controls.Add(this.btn_4);
			this.Controls.Add(this.btn_9);
			this.Controls.Add(this.btn_8);
			this.Controls.Add(this.btn_7);
			this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_7;
        private System.Windows.Forms.Button btn_8;
        private System.Windows.Forms.Button btn_9;
        private System.Windows.Forms.Button btn_6;
        private System.Windows.Forms.Button btn_5;
        private System.Windows.Forms.Button btn_4;
        private System.Windows.Forms.Button btn_3;
        private System.Windows.Forms.Button btn_2;
        private System.Windows.Forms.Button btn_1;
        private System.Windows.Forms.Button btn_0;
        private System.Windows.Forms.Button btn_eq;
        private System.Windows.Forms.Button btn_CE;
        private System.Windows.Forms.Button btn_plus;
        private System.Windows.Forms.Button btn_minus;
        private System.Windows.Forms.Button btn_multiply;
        private System.Windows.Forms.Button btn_devide;
        private System.Windows.Forms.Button btn_dot;
        private System.Windows.Forms.Button btn_abs;
        private System.Windows.Forms.Button btn_log;
        private System.Windows.Forms.Button btn_cos;
        private System.Windows.Forms.Button btn_sin;
        private System.Windows.Forms.Button btn_sqrt;
        private System.Windows.Forms.TextBox rezultat;
    }
}

